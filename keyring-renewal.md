Changing expiry of the amprolla3 key
====================================
(rrq: 2022-09-03)

NOTE: The amprolla3 singing keys are located at amprolla, owned by ~amprolla,
and held in the pgp "homedir" ~/amprolla/gnupg

Log on to amprolla as ~amprolla, and cd to ~/amprolla.  Create a new key or
change the expiry of an existing primary key and of the subkey:

    $ gpg --homedir gnupg/ --edit-key BB23C00C61FC752C
    gpg> key 0
    gpg> expire
    ... 1y
    gpg> key 1
    gpg> expire
    ... 1y
    gpg> save

    $ gpg --homedir gnupg/ -a --export BB23C00C61FC752C > x.gpg

Now x.pgp is the public key that should be imported into the devuan-keyring
package.

Update devuan-keyring
---------------------

Clone this devuan-keyring project. The keyrings are generated from the public
keys with extension `.gpg` in the subdirectories below `public_keys`:

 `individuals` -> devuan-keyring.gpg
 `archive` -> devuan-archive-keyring.gpg
 `removed` -> devuan-removed-keys.gpg

Place the new key (OpenPGP binary format) in a file into the relevant
directory. Obsolete keys can be moved to `removed` rather than deleted.

Note that public keys in the `archive` folder are included in both
`/usr/share/keyrings/devuan-archive-keyring.gpg` and
`/etc/apt/trusted.gpg.d/`. However, `dpkg(1)` treats the latter as
`deb-conffiles(5)` which require special package handling to remove them from
end users' systems. If removal is required, in addition to removing the key from
this directory, add

 `rm_conffile /etc/apt/trusted.gpg.d/<filename> <version> devuan-keyring`

to `d/devuan-keyring.maintscript` as described in `dh_installdeb(1)`.

Existing public keys can be refreshed from [Devuan's
Keyserver](hkps://keyring.devuan.org) by running

    make refresh

Commit changes and update debian/changelog before building the
devuan-keyring package for unstable.
